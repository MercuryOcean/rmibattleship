package com.kozlovbattleship.rmi;

import com.kozlovbattleship.InputInterface.IInputInterface;
import com.kozlovbattleship.OutputInterface.IOutputInterface;
import com.kozlovbattleship.gameobjects.Admiral;
import com.kozlovbattleship.gameobjects.Board;
import com.kozlovbattleship.gameobjects.BoardUnitStatus;
import com.kozlovbattleship.gameobjects.Coordinates;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class RMIServer extends UnicastRemoteObject implements IBattleShips {

    private ArrayList<IBattleShips> clients;
    private int playerCount = 0;
    private final static int port = 1099;

    public RMIServer() throws RemoteException {
        super();
        clients = new ArrayList<>();
    }

    public static void main(String[] args) {
        try {
            IBattleShips service = new RMIServer();
            Registry registry = LocateRegistry.createRegistry(port);
            String serviceName = "BattleShips";
            registry.rebind(serviceName, service);
            service.log("LOG", "Server has started!");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean connect(IBattleShips client) {
        try {
            playerCount++;
            if (playerCount < 3) {
                client.setClientId(playerCount + 1);
                clients.add(client);
                this.log("LOG", "Admiral " + playerCount  + " connected!");
                if (playerCount == 2) {
                    Admiral p1 = new Admiral(clients.get(0));
                    Admiral p2 = new Admiral(clients.get(1));
                    p1.setEnemyAdmiral(p2);
                    p2.setEnemyAdmiral(p1);
                    playGame(p1, p2);
                }
                return true;
            }
            client.log("ERR", "Server is full!");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void log(String type, String msg) {
        if (type.equals("ERR")) {
            System.err.println("[CONNECTION_ERROR]: " + msg);
        } else {
            System.out.println("[" + type + "]: " + msg);
        }
    }

    @Override
    public int getClientId() {
        return 0;
    }

    @Override
    public void setClientId(int id) {
    }

    @Override
    public void showBoards(BoardUnitStatus[][] allyBoardStatuses,
                           BoardUnitStatus[][] enemyBoardStatuses) {
    }

    @Override
    public Coordinates inputShotCoordinates() {
        return new Coordinates(-1, -1);
    }

    private void playGame(Admiral player1, Admiral player2) {
        log("LOG", "Game has started!");
        while (!player1.isLost() && !player2.isLost()) {

            player1.showBoards();
            while(!player1.makeShot()) {
                try {
                    clients.get(0).log("ERR", "Input error!");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            player2.showBoards();
            while(!player2.makeShot()) {
                try {
                    clients.get(1).log("ERR", "Input error!");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
        player1.showResults();
        player2.showResults();
    }
    @Override
    public void showGameResults(boolean win) {
    }
}
