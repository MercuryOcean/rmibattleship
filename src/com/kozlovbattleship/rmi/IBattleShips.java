package com.kozlovbattleship.rmi;

import com.kozlovbattleship.InputInterface.IInputInterface;
import com.kozlovbattleship.OutputInterface.IOutputInterface;
import com.kozlovbattleship.gameobjects.Board;
import com.kozlovbattleship.gameobjects.BoardUnitStatus;
import com.kozlovbattleship.gameobjects.Coordinates;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IBattleShips extends Remote {
    public boolean connect(IBattleShips client)                    throws RemoteException;
    public void setClientId(int id)                                throws RemoteException;
    public int getClientId()                                       throws RemoteException;
    public Coordinates inputShotCoordinates()                      throws RemoteException;
    public void log(String type, String msg)                       throws RemoteException;
    public void showBoards(BoardUnitStatus[][] allyBoardStatuses,
                           BoardUnitStatus[][] enemyBoardStatuses) throws RemoteException;
    public void showGameResults(boolean win)                       throws RemoteException;
}
