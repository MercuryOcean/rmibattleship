package com.kozlovbattleship.rmi;

import com.kozlovbattleship.InputInterface.ConsoleInput;
import com.kozlovbattleship.InputInterface.IInputInterface;
import com.kozlovbattleship.OutputInterface.ConsoleOutput;
import com.kozlovbattleship.OutputInterface.IOutputInterface;
import com.kozlovbattleship.gameobjects.Board;
import com.kozlovbattleship.gameobjects.BoardUnitStatus;
import com.kozlovbattleship.gameobjects.Coordinates;
import com.kozlovbattleship.rmi.IBattleShips;

import java.io.Serializable;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class RMIClient extends UnicastRemoteObject implements IBattleShips, Serializable {

    private final static String serverPath      = "rmi://localhost/BattleShips";
    private IInputInterface     inputInterface  = new ConsoleInput();
    private IOutputInterface    outputInterface = new ConsoleOutput();
    private int                 id;

    public RMIClient() throws RemoteException {
        try {
            IBattleShips server = (IBattleShips) Naming.lookup(serverPath);
            if (!server.connect(this)) {
                log("ERR", "Connection error!");
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            new RMIClient();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean connect(IBattleShips client) {
        return true;
    }

    @Override
    public void setClientId(int id) {
        this.id = id;
    }

    @Override
    public int getClientId() {
        return id;
    }

    @Override
    public void showBoards(BoardUnitStatus[][] allyBoardStatuses, BoardUnitStatus[][] enemyBoardStatuses){
        outputInterface.showBoards(allyBoardStatuses, enemyBoardStatuses);
    }

    @Override
    public Coordinates inputShotCoordinates() {
        return inputInterface.inputParseShotCoord();
    }

    @Override
    public void log(String type, String msg) {
        if (type.equals("ERR")) {
            System.err.println("[CONNECTION_ERROR]: " + msg);
        } else {
            System.out.println("[" + type + "]: " + msg);
        }
    }

    public void showGameResults(boolean win) {
        outputInterface.showGameResults(win);
    }
}
