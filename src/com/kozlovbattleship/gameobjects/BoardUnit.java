package com.kozlovbattleship.gameobjects;

import java.io.Serializable;

public class BoardUnit {

    private BoardUnitStatus status;
    private Ship            containingShip;

    public BoardUnit(BoardUnitStatus status) {
        this.status = status;
    }

    public BoardUnit(BoardUnit unit) {
        this.status    = unit.getStatus();
        containingShip = unit.getContainingShip();
    }

    public BoardUnitStatus getStatus() {
        return status;
    }

    public void setStatus(BoardUnitStatus status) {
        this.status = status;
    }

    public Ship getContainingShip() {return containingShip;};

    public void setContainingShip(Ship containingShip) {
        this.containingShip = containingShip;
    }
}
