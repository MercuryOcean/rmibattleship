package com.kozlovbattleship.gameobjects;

import java.io.Serializable;

public class Ship {

    private BoardUnit[] shipBody;


    public Ship(BoardUnit[] shipBody) {
        this.shipBody = shipBody;
        for (BoardUnit unit : this.shipBody) {
            unit.setStatus(BoardUnitStatus.SHIP);
            unit.setContainingShip(this);
        }
    }

    public boolean update() {
        int damagedCount = 0;
        for (BoardUnit unit : shipBody) {
            if (unit.getStatus() == BoardUnitStatus.DAMAGED_SHIP) {
                damagedCount++;
            }
        }
        if (damagedCount == shipBody.length) {
            for (BoardUnit unit : shipBody) {
                unit.setStatus(BoardUnitStatus.DEAD_SHIP);
            }
            return true;
        }
        return false;
    }

    public boolean isDead() {
        return (shipBody[0].getStatus() == BoardUnitStatus.DEAD_SHIP);
    }
}
