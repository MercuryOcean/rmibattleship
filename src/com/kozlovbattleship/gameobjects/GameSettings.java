package com.kozlovbattleship.gameobjects;

public class GameSettings {

    private int boardSize;
    private int typeNums;
    private ShipTypeSetting[] shipTypeSettings;

    public GameSettings() {
        // TODO!
        //  Need to parse from xml/json or any othere structured text document!
        boardSize = 10;
        typeNums = 4;
        shipTypeSettings = new ShipTypeSetting[typeNums];
        for (int i = 0; i < typeNums; i++) {
            shipTypeSettings[i] = new ShipTypeSetting(i+1, typeNums - i);
        }
    }

    public int getTypeNums() {
        return typeNums;
    }

    public int getBoardSize() {
        return boardSize;
    }

    public ShipTypeSetting[] getShipTypeSetting() {
        ShipTypeSetting[] res = new ShipTypeSetting[shipTypeSettings.length];
        for (int i = 0; i < shipTypeSettings.length; i++) {
            res[i] = new ShipTypeSetting(shipTypeSettings[i]);
        }
        return res;
    }
}
