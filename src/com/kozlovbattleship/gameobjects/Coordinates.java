package com.kozlovbattleship.gameobjects;

import java.io.Serializable;

public class Coordinates implements Serializable {

    private int row;
    private int column;

    public Coordinates(int row, int column) {
        this.row    = row;
        this.column = column;
    }

    public Coordinates(Coordinates coordinates) {
        this.row    = coordinates.getRow();
        this.column = coordinates.getColumn();
    }

    public int[] getRowColumn() {
        int res[] = new int[2];
        res[0] = row;
        res[1] = column;
        return res;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
