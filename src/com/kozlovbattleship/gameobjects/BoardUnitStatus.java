package com.kozlovbattleship.gameobjects;

public enum BoardUnitStatus {
    UNDEFINED,
    EMPTY,
    SHIP,
    MISS,
    DAMAGED_SHIP,
    DEAD_SHIP
}
