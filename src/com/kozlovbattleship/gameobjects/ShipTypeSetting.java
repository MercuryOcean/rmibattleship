package com.kozlovbattleship.gameobjects;

import javafx.util.Pair;

import java.io.Serializable;

public class ShipTypeSetting {

    private Pair<Integer, Integer> shipTypeSetting;

    public ShipTypeSetting(ShipTypeSetting shipTypeSetting) {
        this.shipTypeSetting = new Pair<>(
                shipTypeSetting.getShipTypeNum(),
                shipTypeSetting.getShipTypeLen()
        );
    }

    public ShipTypeSetting(int shipTypeNum, int shipTypeLen) {
        shipTypeSetting = new Pair<Integer, Integer>(shipTypeNum, shipTypeLen);
    }

    public int getShipTypeNum() {
        return shipTypeSetting.getKey();
    }

    public Integer getShipTypeLen() {
        return shipTypeSetting.getValue();
    }
}
