package com.kozlovbattleship.gameobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Board {

    private GameSettings    gameSettings;
    private ArrayList<Ship> navy;
    private BoardUnit[][]   boardBody;

    public Board() {
        navy = new ArrayList<>();
        gameSettings = new GameSettings();
        boardBody = new BoardUnit[gameSettings.getBoardSize()][gameSettings.getBoardSize()];
        for (int i = 0; i < gameSettings.getBoardSize(); i++) {
            for (int j = 0; j < gameSettings.getBoardSize(); j++) {
                boardBody[i][j] = new BoardUnit(BoardUnitStatus.EMPTY);
            }
        }
            // TODO!
            //  Manual ship positioning
            setBattleshipsRandomly();
    }

    public BoardUnit[][] getBoardBody() {
        return boardBody;
    }

    public GameSettings getGameSettings() {
        return gameSettings;
    }

    public BoardUnitStatus recieveShot(int row, int column) {
        BoardUnitStatus targetStatus = boardBody[row][column].getStatus();
        if (targetStatus == BoardUnitStatus.EMPTY) {
            boardBody[row][column].setStatus(BoardUnitStatus.MISS);
            return boardBody[row][column].getStatus();
        } else if (targetStatus == BoardUnitStatus.SHIP) {
            boardBody[row][column].setStatus(BoardUnitStatus.DAMAGED_SHIP);
            boardBody[row][column].getContainingShip().update();
            return boardBody[row][column].getStatus();
        } else {
            return BoardUnitStatus.UNDEFINED;
        }
    }

    public BoardUnitStatus recieveShot(Coordinates coordinates) {
        return recieveShot(coordinates.getRow(), coordinates.getColumn());
    }

    public int size() {
        return boardBody.length;
    }

    public ArrayList<Ship> getNavy() {
        return navy;
    }

    private void setBattleshipsRandomly() {
        for (int i = 0; i < gameSettings.getTypeNums(); i++) {
            for (int j = 0; j < gameSettings.getShipTypeSetting()[i].getShipTypeNum(); j++) {
                createShipRandomPos(gameSettings.getShipTypeSetting()[i].getShipTypeLen());
            }
        }
    }

    private void createShipRandomPos(int shipLen) {
        Random  random = new Random();
        boolean canBeSetHere = false;
        int row = 0;
        int column = 0;
        int mul = 0;
        boolean vertical = false;
        while (!canBeSetHere) {
            row = random.nextInt(gameSettings.getBoardSize());
            column = random.nextInt(gameSettings.getBoardSize());
            vertical = random.nextBoolean();
            mul = vertical ? 1 : 0;
            if (row + (shipLen - 1) * mul  < gameSettings.getBoardSize() &&
                    column + (shipLen - 1) * (1-mul)  < gameSettings.getBoardSize()) {
                canBeSetHere = true;
                for (int i = 0; i < shipLen; i++) {
                    if (isAnyShipNearUnit(row + i * mul, column + i * (1 - mul))) {
                        canBeSetHere = false;
                    }
                }
            }
        }
        BoardUnit[] shipBody = new BoardUnit[shipLen];
        for (int i = 0; i < shipLen; i++) {
            shipBody[i] = boardBody[row + i * mul][column + i * (1 - mul)];
        }
        navy.add(new Ship(shipBody));
    }

    private boolean isAnyShipNearUnit(int row, int column) {
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                boolean inBounds = row + i >= 0 && column + j >= 0 &&
                        row + i < gameSettings.getBoardSize() &&
                        column + j < gameSettings.getBoardSize();
                if (inBounds) {
                    if (boardBody[row + i][column + j].getStatus() ==
                            BoardUnitStatus.SHIP) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}