package com.kozlovbattleship.gameobjects;
import com.kozlovbattleship.rmi.IBattleShips;

import java.rmi.RemoteException;

public class Admiral {

    private Board        allyBoard;
    private IBattleShips player;
    private Admiral      enemyAdmiral;

    public Admiral(IBattleShips player) {
        allyBoard   = new Board();
        enemyAdmiral  = null;
        this.player = player;
    }

    public Board getAllyBoard() {
        return allyBoard;
    }

    private BoardUnitStatus recieveShot(int row, int column) {
        return allyBoard.recieveShot(row, column);
    }

    private BoardUnitStatus recieveShot(Coordinates coordinates) {
        return allyBoard.recieveShot(coordinates);
    }

    private Coordinates inputShotCoord() {
        Coordinates res;
        try {
            res = player.inputShotCoordinates();
        } catch (RemoteException e) {
            res = new Coordinates(-1, -1);
            e.printStackTrace();
        }
        return res;
    }

    public void showBoards() {
        BoardUnitStatus[][] allyBoardStatuses = new BoardUnitStatus[allyBoard.size()][allyBoard.size()];
        BoardUnitStatus[][] enemyBoardStatuses = new BoardUnitStatus[allyBoard.size()][allyBoard.size()];
        for (int i = 0; i < allyBoard.size(); i++) {
            for (int j = 0; j < allyBoard.size(); j++) {
                allyBoardStatuses[i][j] = allyBoard.getBoardBody()[i][j].getStatus();
                enemyBoardStatuses[i][j] = enemyAdmiral.getAllyBoard().getBoardBody()[i][j].getStatus();
            }
        }
        try {
            player.showBoards(allyBoardStatuses, enemyBoardStatuses);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public boolean isLost() {
        int intactShips = allyBoard.getNavy().size();
        for (Ship ship : allyBoard.getNavy()) {
            if (ship.isDead()) {
                intactShips--;
            }
        }
        return (intactShips == 0);
    }

    public boolean makeShot() {
        BoardUnitStatus status = enemyAdmiral.getAllyBoard().recieveShot(inputShotCoord());
        if (status == BoardUnitStatus.UNDEFINED) {
            return false;
        }
        return true;
    }

    public void setEnemyAdmiral(Admiral enemyAdmiral) {
        this.enemyAdmiral = enemyAdmiral;
    }

    public void showResults() {
        try {
            player.showGameResults(!isLost());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
