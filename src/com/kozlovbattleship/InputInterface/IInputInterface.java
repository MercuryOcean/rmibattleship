package com.kozlovbattleship.InputInterface;

import com.kozlovbattleship.gameobjects.Coordinates;

public interface IInputInterface {
    public Coordinates inputParseShotCoord();
}
