package com.kozlovbattleship.InputInterface;

import com.kozlovbattleship.gameobjects.Coordinates;
import com.kozlovbattleship.gameobjects.GameSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ConsoleInput implements IInputInterface {

    @Override
    public Coordinates inputParseShotCoord() {

        GameSettings settings = new GameSettings();
        Scanner scanner = new Scanner(System.in);
        String unparsedCoord = scanner.nextLine();
        String parsedCoord = unparsedCoord.replaceAll("[^-?0-9]+", " ");
        ArrayList<String> list = new ArrayList<>(Arrays.asList(parsedCoord.trim().split(" ")));
        if ( list.size() < 2) {
            return new Coordinates(-1, -1);
        }

        int rres =  Integer.parseInt(list.get(0));
        int cres = Integer.parseInt(list.get(1));

        if (rres < 0 || rres > settings.getBoardSize() ||
                cres < 0 || cres > settings.getBoardSize()) {
            return new Coordinates(-1, -1);
        }

        return new Coordinates(rres,cres);
    }
}
