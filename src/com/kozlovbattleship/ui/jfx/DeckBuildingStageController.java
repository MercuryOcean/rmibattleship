package com.kozlovbattleship.ui.jfx;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.*;

import javafx.scene.control.Button;
import javafx.scene.paint.Color;

public class DeckBuildingStageController {
    @FXML
    private Pane submarine;

    @FXML
    private Pane destroyer;

    @FXML
    private Pane cruiser;

    @FXML
    private Pane battleship;

    @FXML
    private GridPane deck;

    @FXML
    private Button startButton;

    @FXML
    private RadioButton vertical;

    @FXML
    private RadioButton horizontal;

    @FXML
    public void initialize() {

        createRadioButtonToggleGroup();
        for (int i = 0; i < deck.getColumnCount(); i++) {
            for (int j = 0; j < deck.getRowCount(); j++) {
                addPane(i, j);
            }
        }
    }

    private void addPane(int colIndex, int rowIndex) {
        Pane pane = new Pane();
        pane.setOnMouseEntered(e -> {
            pane.setBackground(new Background(new BackgroundFill(Color.web("#FFFF00"), CornerRadii.EMPTY, Insets.EMPTY)));
        });
            deck.add(pane, colIndex, rowIndex);
    }

    private void createRadioButtonToggleGroup() {
        ToggleGroup orientationGroup = new ToggleGroup();
        vertical.setToggleGroup(orientationGroup);
        horizontal.setToggleGroup(orientationGroup);
    }

    private void createShipToggleGroup() {

    }

}

