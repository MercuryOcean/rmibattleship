package com.kozlovbattleship.ui.jfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Battleship extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent deckBuildingStageScene = FXMLLoader.load(getClass().getResource("DeckBuildingStage.fxml"));

        primaryStage.setTitle("login");
        primaryStage.setScene(new Scene(deckBuildingStageScene, 600, 402));

        primaryStage.show();
    }
}
