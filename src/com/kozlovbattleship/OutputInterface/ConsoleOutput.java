package com.kozlovbattleship.OutputInterface;

import com.kozlovbattleship.gameobjects.BoardUnitStatus;

public class ConsoleOutput implements IOutputInterface {

    @Override
    public void showBoards(BoardUnitStatus[][] allyBoardStatuses, BoardUnitStatus[][] enemyBoardStatuses) {
        showParsedBoards(parseBoard(allyBoardStatuses, true),
                parseBoard(enemyBoardStatuses, false));
    }

    @Override
    public void showParsedBoards(Object parsedAllyBoard, Object parsedEnemyBoard) {
        String[] ally = ((String[]) parsedAllyBoard);
        String[] enemy = ((String[]) parsedEnemyBoard);
        String res = "";
        for (int tap = 0; tap < 2; tap++) {
            String[] parsedBoard;
            if (tap == 0) {
                res += "\n\n\t Your board\n";
                parsedBoard = ally;
            } else {
                res += "\n\n\t Enemy board\n";
                parsedBoard = enemy;
            }
            for (int i = -1; i < parsedBoard.length; i++) {
                if (i == -1) {
                    for (int j = 0; j < ally.length; j++) {
                        res += j + " ";
                    }
                    res += "\n\n";
                } else {
                    for (int j = -1; j < ally.length; j++) {
                        if (j == -1) {
                            res += i + "    ";
                        } else {
                            res += parsedBoard[i].charAt(j) + " ";
                        }
                    }
                    res += "\n";
                }
            }
            res += "\n";
        }
        System.out.print(res);
    }

    @Override
    public Object parseBoard(BoardUnitStatus[][] boardStatuses, boolean allyboard) {
        String[] res = new String[boardStatuses.length];
        for (int i = 0; i < boardStatuses.length; i++) {
            res[i] = "";
            for (int j = 0; j < boardStatuses.length; j++) {
                switch (boardStatuses[i][j]) {
                    case UNDEFINED:
                        res[i] += "_";
                        break;
                    case EMPTY:
                        res[i] += allyboard ? " " : "_";
                        break;
                    case SHIP:
                        res[i] += allyboard ? "S" : "_";
                        break;
                    case DAMAGED_SHIP:
                        res[i] += "x";
                        break;
                    case DEAD_SHIP:
                        res[i] = "X";
                        break;
                    case MISS:
                        res[i] = "*";
                        break;
                }
            }
        }
        return res;
    }

    @Override
    public void showGameResults(boolean win) {
        String res = win ? "You win!" : "You lose!";
        System.out.println(res);
    }

    @Override
    public void showGameStatus(boolean yourTurn) {
        String res = yourTurn ? "Your turn:" : "Enemy's turn. Waiting...";
        System.out.println(res);
    }
}
