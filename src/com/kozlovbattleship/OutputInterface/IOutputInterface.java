package com.kozlovbattleship.OutputInterface;

import com.kozlovbattleship.gameobjects.Board;
import com.kozlovbattleship.gameobjects.BoardUnitStatus;

public interface IOutputInterface {
    public void showBoards(BoardUnitStatus[][] allyBoardStatuses, BoardUnitStatus[][] enemyBoardStatuses);
    public void showParsedBoards(Object parsedAllyBoard, Object parsedEnemyBoard);
    public Object parseBoard(BoardUnitStatus[][] boardStatuses, boolean allyboard);
    public void showGameResults(boolean win);
    public void showGameStatus(boolean yourTurn);
}
